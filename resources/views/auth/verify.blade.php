@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 d-none d-md-block bg-secondary full-height greeting"></div>
        <div class="col-md-8">
            <div class="text-right">
                <p class="mt-3">Not a member? <a href="{{route('register')}}">Sign up now</a></p>
                <p class="mb-5">Already a member? <a href="{{route('login')}}">Sign In</a></p>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 mt-5">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@extends('layouts.app')
