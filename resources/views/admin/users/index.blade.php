@extends('layouts.admin')

@section('content')
<h2>Список пользователей</h2>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>E-mail</th>
            <th>Photos</th>
            <th>Actions</th>
        </tr>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>0</td>
            <td>
                <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
            </td>
        </tr>
        @endforeach
    </table>
</div>
@stop