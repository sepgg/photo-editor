@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-9"><h5>Редактирование пользователя <b>{{$user->email}}</b></h5></div>
    <div class="col-md-3 text-right">
        <form action="{{route('admin.users.destroy', $user->id)}}" method="post" class="confirmed d-inline">
            @csrf
            <input type="hidden" name="_method" value="DELETE">
            <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Удалить</button>
        </form>
    </div>
</div>
<hr>
<form action="{{route('admin.users.update', $user->id)}}" method="POST">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <div class="row">
        @foreach($filters as $f)
            <div class="col">
                <label>{{$f->name}}</label>
                <input type="text" class="form-control" name="q[{{$f->id}}]" value="{{$userFilters[$f->id] ?? 0}}">
            </div>
        @endforeach
    </div>
    <hr>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="active" @if($user->active) checked @endif @if($user->id == 1) disabled @endif>
        <label class="form-check-label" for="active">Активность</label>
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="admin" @if($user->admin) checked @endif @if($user->id == 1) disabled @endif>
        <label class="form-check-label" for="admin">Сделать администратором</label>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input name="name" disabled value="{{old('name') ? old('name') : $user->name}}" type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input name="email" disabled value="{{old('email') ? old('email') : $user->email}}" type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-primary btn-sm" type="submit">Сохранить</button>
    </div>
</form>
<hr>
<h4>Фото пользователя</h4>
@if($user->works->count())
<works :works="{{$user->works}}"></works>
@else
    Не найдено
@endif
@stop