@extends('layouts.app')

@section('content')
<editor :user="{{auth()->user()}}" :categories="{{$categories}}"></editor>
@endsection
