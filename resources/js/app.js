require('./bootstrap');

window.Vue = require('vue').default;

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


const app = new Vue({
    el: '#app',
});

$(document).ready(function(){
    $('form.confirmed').submit(function(e){
        if(!confirm('Подтвердите действие')){
            e.preventDefault()
            return false
        }
    })
})