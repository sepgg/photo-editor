<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('filters')->truncate();
        DB::table('filter_categories')->truncate();
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('users')->insert([
            [ 'name' => 'Admin', 'active' => 1, 'admin' => 1, 'email' => 'admin@email.net', 'password' => bcrypt('password') ]
        ]);
        DB::table('filter_categories')->insert([
            [ 'name' => 'Восстановление' ],
            [ 'name' => 'Контур' ],
            [ 'name' => 'Haar' ]
        ]);
        DB::table('filters')->insert([
            [ 'filter_category_id' => 1, 'name' => '_VOSS' ],
            [ 'filter_category_id' => 1, 'name' => '_SHUM' ],
            [ 'filter_category_id' => 2, 'name' => '_KONT' ],
            [ 'filter_category_id' => 2, 'name' => '_TEN' ],
            [ 'filter_category_id' => 2, 'name' => '_RAZM' ],
            [ 'filter_category_id' => 3, 'name' => '_VOL' ],
            [ 'filter_category_id' => 3, 'name' => '_DETAL' ]
        ]);
    }
}
