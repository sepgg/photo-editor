<?php

use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::group([
    'middleware' => ['auth', 'verified']
], function(){
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::post('/upload', [App\Http\Controllers\FileController::class, 'upload']);
    Route::post('/update/{work}', [App\Http\Controllers\FileController::class, 'update']);
    Route::get('/download/{work}/{type?}', [App\Http\Controllers\FileController::class, 'download']);
});

Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin'],
    'as' => 'admin.'
], function(){
    Route::view('/', 'admin.index')->name('index');
    Route::resource('/users', App\Http\Controllers\UserController::class);
});
