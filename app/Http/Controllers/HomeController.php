<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FilterCategory;
class HomeController extends Controller
{
    public function index()
    {
        $categories = FilterCategory::all();
        return view('home', compact('categories'));
    }
}
