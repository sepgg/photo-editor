<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use App\Models\Work;
use App\Models\Filter;
use DB;
use Illuminate\Support\Str;
use Storage;
class FileController extends Controller
{
    public function upload(Request $request){
        $request->validate([
            'file' => 'required|image'
        ]);
        $file = $request->file;
        $fullname = $file->getClientOriginalName();
        $name = pathinfo($fullname, PATHINFO_FILENAME);
        $ext = pathinfo($fullname, PATHINFO_EXTENSION);
        $workFile = File::create([
            'name' => $fullname,
            'src' => $file->store('photos/' . auth()->id()),
            'originalName' => $name,
            'ext' => $ext
        ]);
        $work = Work::create([
            'user_id' => auth()->id(),
            'original_file_id' => $workFile->id
        ]);
        $work->load('original', 'user', 'filters');
        return $work;
    }
    public function update(Request $request, Work $work){
        $uid = auth()->id();
        $userFilters = [];
        foreach(auth()->user()->filters as $f){
            $userFilters[$f->id] = $f->pivot->quantity;
        }
        if($work->user_id != $uid) return false;
        $errors = [];
        $filters = Filter::whereIn('id', $request->filters)->get();
        foreach($filters as $filter){
            if(!isset($userFilters[$filter->id]) || $userFilters[$filter->id] < 1){
                $errors = [ 'errors' => [ 'balance' => 'Применение одного из фильтров не доступно, проверьте баланс' ]];
                break;
            }
        }
        if(count($errors)) return response()->json($errors, 422);
        if($work->modified){
            $newName = Str::random(40);
            $originalFile = $request->target == 'o' ? $work->original : $work->modified;
            $newSrc = 'photos/' . auth()->id() . '/' . $newName . '.' . $originalFile->ext;
            Storage::copy($originalFile->src, $newSrc);
            $workFile = File::create([
                'name' => $originalFile->name,
                'src' => $newSrc,
                'originalName' => $originalFile->originalName,
                'ext' => $originalFile->ext
            ]);
            $work = Work::create([
                'user_id' => auth()->id(),
                'original_file_id' => $workFile->id
            ]);
            $work->load('original');
        }
        $name = $work->original->originalName;
        DB::table('filter_user')->where('user_id', $uid)->whereIn('filter_id', $request->filters)->decrement('quantity');
        foreach($filters as $filter){
            $name .= $filter->name;
        }
        $name .= '.' . $work->original->ext;
        $fileparts = explode('/', $work->original->src);
        $src = $fileparts[0] . '/' . $fileparts[1] . '/m_' . $fileparts[2];
        Storage::copy($work->original->src, $src);
        $newFile = File::create([
            'name' => $name,
            'src' => $src,
            'originalName' => $work->original->originalName,
            'ext' => $work->original->ext
        ]);
        $work->filters()->sync($request->filters);
        $work->modified_file_id = $newFile->id;
        $work->save();
        $work->load('modified', 'filters', 'user');
        return $work;
    }
    public function download(Work $work, $type = false){
        $uid = auth()->id();
        if($work->user_id != $uid || !auth()->user()->admin) abort(403);
        if($type == 'original')
            return Storage::download($work->original->src, $work->original->name);
        elseif($type == 'modified' && $work->modified)
            return Storage::download($work->modified->src, $work->modified->name);
        return back();
    }
}
