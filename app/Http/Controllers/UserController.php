<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\FilterCategory;
use App\Models\Filter;
class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(25);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    
    public function edit(User $user)
    {
        $filters = Filter::all();
        $userFilters = [];
        foreach($user->filters as $f){
            $userFilters[$f->id] = $f->pivot->quantity;
        }
        return view('admin.users.edit', compact('user', 'filters', 'userFilters'));
    }

    public function update(Request $request, User $user)
    {
        $syncData = [];
        foreach($request->q as $key => $val){
            $syncData[$key] = [
                'quantity' => $val
            ];
        }
        $user->filters()->sync( $syncData );
        if($request->active) $user->active = 1;
        if($request->admin) $user->admin = 1;
        $user->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
