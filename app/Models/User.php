<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'active',
        'admin'
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $with = ['filters', 'works'];
    public function isAdmin(){
        return $this->admin;
    }
    public function works(){
        return $this->hasMany(Work::class);
    }
    public function filters(){
        return $this->belongsToMany(Filter::class)->withPivot('quantity');
    }
}
