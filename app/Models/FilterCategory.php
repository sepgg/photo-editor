<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilterCategory extends Model
{
    public $timestamps = false;
    protected $with = ['filters'];
    public function filters(){
        return $this->hasMany(Filter::class);
    }
}
