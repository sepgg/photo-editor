<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    public $timestamps = false;
    public function category(){
        return $this->belongsTo(FilterCategory::class);
    }
}
