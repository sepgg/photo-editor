<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'src', 'originalName', 'ext'];
    protected $casts = [
        'created_at' => 'datetime:d/m/Y H:i'
    ];
}
