<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = ['user_id', 'original_file_id', 'modified_file_id'];
    protected $with = ['original', 'modified', 'filters'];
    public function original(){
        return $this->belongsTo(File::class, 'original_file_id');
    }
    public function modified(){
        return $this->belongsTo(File::class, 'modified_file_id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function filters(){
        return $this->belongsToMany(Filter::class);
    }
}
